import React from 'react';

const NumbersItem = (props) => {
  return <li className="numbers-list__item">{props.number}</li>;
};

export default NumbersItem;