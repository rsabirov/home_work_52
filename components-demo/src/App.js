import React, { Component } from 'react';
import './App.css';
import './bootstrap.min.css';
import NumbersItem from './components/NumbersItem'

class App extends Component {
  state = {
    numbers: []
  };

  generateRandomNumber = () => {
    return Math.floor(Math.random() * (36 - 5 + 1)) + 5;
  };

  handleClick = () => {
    const numbers = [];
    let i = 0;

    while (i < 5) {
      let number = this.generateRandomNumber();
      if (numbers.indexOf(number) === -1) {
        i++;
        numbers.push(number);
      }
    }

    numbers.sort((a, b) => (a - b));

    this.setState({numbers});
  };

  render() {
    return (
      <div className="App">
        <div className="container">
          <button onClick={this.handleClick} className="btn btn-primary btn-lg">New numbers</button>
          <ul className="d-flex justify-content-center numbers-list">
            {this.state.numbers.map(number => <NumbersItem number={number} key={number}/>)}
          </ul>
        </div>
      </div>
    );
  }
}

export default App;
